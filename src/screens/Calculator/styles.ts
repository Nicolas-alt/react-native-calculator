import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  textResult: {
    color: 'white',
    fontSize: 60,
    textAlign: 'right',
    fontWeight: '100',
  },
  textSubResult: {
    color: 'rgba(255,255,255, 0.5)',
    fontSize: 30,
    textAlign: 'right',
    fontWeight: '100',
  },
  resultLayout: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  layout: {
    flex: 1,
    flexDirection: 'row',
  },
});
