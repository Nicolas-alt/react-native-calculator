import React from 'react';
import {Text, View} from 'react-native';
import Row from '../../components/Row/Row';
import Button from '../../components/Button/Button';
import {useCalculator} from '../../hooks/useCalculator';
import {styles} from './styles';

const Calculator: React.FC = () => {
  const {
    division,
    multiply,
    addition,
    operation,
    subtraction,
    lastNum,
    currentOp,
    currentNumber,
    handleReset,
    porcentage,
    handlePlus,
    putNumbersTogether,
    deleteLastInput,
  } = useCalculator();

  return (
    <View style={styles.container}>
      <View style={styles.layout}>
        <Text style={styles.textSubResult}>{currentOp}</Text>
        <View style={styles.resultLayout}>
          {lastNum !== '0' && (
            <Text style={styles.textSubResult}>{lastNum}</Text>
          )}
          <Text
            style={styles.textResult}
            numberOfLines={1}
            adjustsFontSizeToFit>
            {currentNumber}
          </Text>
        </View>
      </View>
      <Row>
        <Button color="2" action={handleReset}>
          C
        </Button>
        <Button color="2" action={porcentage}>
          %
        </Button>
        <Button color="2" action={handlePlus}>
          +/-
        </Button>
        <Button color="3" action={division}>
          /
        </Button>
      </Row>

      <Row>
        <Button action={putNumbersTogether}>7</Button>
        <Button action={putNumbersTogether}>8</Button>
        <Button action={putNumbersTogether}>9</Button>
        <Button color="3" action={multiply}>
          X
        </Button>
      </Row>

      <Row>
        <Button action={putNumbersTogether}>4</Button>
        <Button action={putNumbersTogether}>5</Button>
        <Button action={putNumbersTogether}>6</Button>
        <Button color="3" action={subtraction}>
          -
        </Button>
      </Row>

      <Row>
        <Button action={putNumbersTogether}>1</Button>
        <Button action={putNumbersTogether}>2</Button>
        <Button action={putNumbersTogether}>3</Button>
        <Button action={addition} color="3">
          +
        </Button>
      </Row>

      <Row>
        <Button action={putNumbersTogether}>0</Button>
        <Button action={putNumbersTogether}>.</Button>
        <Button action={deleteLastInput}>Del</Button>
        <Button action={operation} color="3">
          =
        </Button>
      </Row>
    </View>
  );
};

export default Calculator;
