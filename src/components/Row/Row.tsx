import React from 'react';
import {View} from 'react-native';
import {styles} from './styles';

const Row: React.FC = ({children}) => {
  return <View style={styles.row}>{children}</View>;
};

export default Row;
