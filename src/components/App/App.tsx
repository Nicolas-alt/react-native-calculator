import React from 'react';
import Theme from '../Theme/Theme';
import Calculator from '../../screens/Calculator/Calculator';

const App: React.FC = () => {
  return (
    <Theme>
      <Calculator />
    </Theme>
  );
};
export default App;
