import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import {styles} from './styles';

const Theme: React.FC = ({children}) => {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor="black" barStyle="light-content" />
      {children}
    </SafeAreaView>
  );
};

export default Theme;
