import {StyleSheet} from 'react-native';

const buttonBaseStyles = StyleSheet.create({
  value: {
    height: 80,
    width: 80,
    borderRadius: 100,
    justifyContent: 'center',
    alignContent: 'center',
    marginHorizontal: 5,
  },
});

const textBaseStyles = StyleSheet.create({
  value: {
    textAlign: 'center',
    fontSize: 30,
  },
});

export const styles = StyleSheet.create({
  textBlack: {...textBaseStyles.value, color: 'black'},
  textWhite: {...textBaseStyles.value, color: 'white'},
  buttonDarkGray: {
    ...buttonBaseStyles.value,
    backgroundColor: '#2D2D2D',
  },
  buttonSoftGray: {
    ...buttonBaseStyles.value,
    backgroundColor: '#9B9B9B',
  },
  buttonOrange: {
    ...buttonBaseStyles.value,
    backgroundColor: '#FF9427',
  },
});
