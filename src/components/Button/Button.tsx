import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {styles} from './styles';

interface ButtonInterface {
  color?: string;
  action: (args: string) => void;
}

const Button: React.FC<ButtonInterface> = ({children, color, action}) => {
  let buttonStyleType;
  let textStyleType;

  switch (color) {
    case '1':
      buttonStyleType = styles.buttonDarkGray;
      textStyleType = styles.textWhite;
      break;
    case '2':
      buttonStyleType = styles.buttonSoftGray;
      textStyleType = styles.textWhite;
      break;
    case '3':
      buttonStyleType = styles.buttonOrange;
      textStyleType = styles.textWhite;
      break;

    default:
      buttonStyleType = styles.buttonSoftGray;
      textStyleType = styles.textWhite;
      break;
  }

  return (
    <TouchableOpacity onPress={() => action(children as string)}>
      <View style={buttonStyleType}>
        <Text style={textStyleType}>{children}</Text>
      </View>
    </TouchableOpacity>
  );
};

Button.defaultProps = {
  color: '1',
};

export default Button;
