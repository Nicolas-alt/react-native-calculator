import {useRef, useState} from 'react';

export const useCalculator = () => {
  enum Operations {
    division,
    addition,
    porcentage,
    subtraction,
    multiplication,
  }

  const lastOp = useRef<Operations>();
  const initialState = '0';
  const [lastNum, setLastNum] = useState(initialState);
  const [currentNumber, setCurrentNumber] = useState(initialState);
  const [currentOp, setCurrentOp] = useState('');

  const handleReset = () => {
    setCurrentNumber(initialState);
    setLastNum(initialState);
    setCurrentOp('');
  };

  const putNumbersTogether = (num: string) => {
    if (currentNumber.includes('.') && num === '.') return;

    if (currentNumber.startsWith('0') || currentNumber.startsWith('-0')) {
      if (num === '.') {
        setCurrentNumber(currentNumber + num);
      } else if (num === '0' && currentNumber.includes('.')) {
        setCurrentNumber(currentNumber + num);
      } else if (num !== '0' && !currentNumber.includes('.')) {
        setCurrentNumber(num);
      } else if (num === '0' && !currentNumber.includes('.')) {
        setCurrentNumber(currentNumber);
      } else {
        setCurrentNumber(currentNumber + num);
      }
    } else {
      setCurrentNumber(currentNumber + num);
    }
  };

  const handlePlus = () => {
    if (currentNumber.includes('-')) {
      setCurrentNumber(currentNumber.replace('-', ''));
    } else {
      setCurrentNumber('-' + currentNumber.replace('-', ''));
    }
  };

  const deleteLastInput = () => {
    let isNegative = '';
    let aux = currentNumber;

    if (currentNumber.includes('-')) {
      isNegative = '-';
      aux = currentNumber.substr(1);
    }

    if (aux.length > 1) {
      setCurrentNumber(isNegative + aux.slice(0, -1));
    } else {
      setCurrentNumber('0');
    }
  };

  const changeCurrentValue = () => {
    if (currentNumber.endsWith('.')) {
      setCurrentNumber(currentNumber.slice(0, -1));
    } else {
      setLastNum(currentNumber);
    }
    setCurrentNumber('0');
  };

  const addition = () => {
    changeCurrentValue();
    setCurrentOp('+');
    lastOp.current = Operations.addition;
  };

  const subtraction = () => {
    changeCurrentValue();
    setCurrentOp('-');
    lastOp.current = Operations.subtraction;
  };

  const multiply = () => {
    changeCurrentValue();
    setCurrentOp('x');
    lastOp.current = Operations.multiplication;
  };

  const division = () => {
    changeCurrentValue();
    setCurrentOp('/');
    lastOp.current = Operations.division;
  };

  const porcentage = () => {
    changeCurrentValue();
    setCurrentOp('%');
    lastOp.current = Operations.porcentage;
  };

  const operation = () => {
    const number1 = Number(currentNumber);
    const number2 = Number(lastNum);

    switch (lastOp.current) {
      case Operations.addition:
        setCurrentNumber(`${number1 + number2}`);
        break;

      case Operations.subtraction:
        setCurrentNumber(`${number2 - number1}`);
        break;

      case Operations.division:
        setCurrentNumber(`${number2 / number1}`);
        break;

      case Operations.multiplication:
        setCurrentNumber(`${number1 * number2}`);
        break;

      case Operations.porcentage:
        setCurrentNumber(`${(number1 * number2) / 100}`);
        break;
    }
    setLastNum('0');
    setCurrentOp('');
  };

  return {
    lastNum,
    currentOp,
    currentNumber,
    addition,
    multiply,
    division,
    operation,
    porcentage,
    subtraction,
    handlePlus,
    handleReset,
    deleteLastInput,
    putNumbersTogether,
  };
};
