# Calculator - React Native

Mobile application made in [React Native](https://reactnative.dev/) & [Typescript](https://www.typescriptlang.org/)

## 🔧 Setup
Install the local dependencies 📦, only the first time. 👉 **You must have [NodeJs](https://nodejs.org/en/) >= 12** 👈
```bash
npm install 
```

## 🏠 Run local
```bash
# Run emulator
npx react-native run-android


# Run Metro
npm start

```


## 🧪 Run test
```bash
npm test
```
## Final result

<div align="center">
    <img src="./final.png">
</div>